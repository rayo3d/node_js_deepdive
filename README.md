# README #

NodeJS Deep Dive set of courses from Pluralsight

### Global NPM Packages ###

* NSP - npm install -g nsp
* LocalTunnel - npm install -g localtunnel

### Running Project ###

* npm start
 * Run start command as defined in Scripts inside package.json
* npm start -s
  * Run start command as defined in Scripts inside package.json in Silent Mode to remove some messaging
